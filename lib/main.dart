import 'package:flutter/material.dart';
import 'package:listforms/formitem.dart';
import 'package:listforms/listforms.dart';
import 'package:listforms/user.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  List<UserForm> forms = List<UserForm>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
        debugShowCheckedModeBanner: false,
      home: Scaffold(

            body:  ListForms(forms: forms,)
         ));
  }
}
  //validate from
 /* bool validate(){
    var valid = form .currentState.validate();
    if(valid) form.currentState.save();
    return valid;
  }*/

