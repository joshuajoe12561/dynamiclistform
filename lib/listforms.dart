import 'package:flutter/material.dart';
import 'package:listforms/formitem.dart';
import 'package:listforms/main.dart';
import 'package:listforms/user.dart';

class ListForms extends StatefulWidget {
  ListForms({this.forms});
  List<UserForm> forms;

  @override
  _ListFormsState createState() => _ListFormsState();
}

class _ListFormsState extends State<ListForms> {
  //List<User> users = [];
  List<UserForm> formss ;
  List<User> dataList=[];
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text('Users Form'),
        actions: <Widget>[
          FlatButton(
            onPressed:onSave,
            child: Text('save'),)
        ],
      ),
      //widget.forms.length
      body: widget.forms.length<=0 ? Center(
        child: Text('Please add a form'),
              ):ListView.builder(
                  addAutomaticKeepAlives: true,
                  itemCount: widget.forms.length,
                  itemBuilder: (_, i)=> widget.forms[i],//UserForm(key: GlobalKey(),formUser: dataList[i],onRemove: ()=>onRemove(dataList[i]),),

                  ),
      floatingActionButton: FlatButton(
          color: Colors.deepPurple,
          child: Text('Add Form'),
          onPressed: (){
            onAddForm();
          },),
    );
  }
  void onRemove(User user){
    setState(() {
      var find = widget.forms.firstWhere(
            (it) => it.formUser == user,
        orElse: () => null,
      );
      if (find != null){
        widget.forms.removeAt(widget.forms.indexOf(find));
        //dataList.remove(find.formUser);
      }

      //dataList.remove(user);

    });
  }
  void onAddForm(){
    setState(() {
      User _user=User();
      //dataList.add(_user);
      //if(widget.forms.length<3)
        widget.forms.add(UserForm(
          key: GlobalKey(),
          formUser: _user,
          onRemove: ()=>onRemove(_user)));
      /*else{
        onAddValidation();
        widget.forms.clear();
        widget.forms.add(UserForm(
            key: GlobalKey(),
            formUser: _user,
            onRemove: ()=>onRemove(_user)));
      }*/
      });
  }
  void onAddValidation(){
    if (widget.forms.length > 0) {
      var allValid = true;
     // widget.forms.map((it) => it.formUser).toList();
      for(UserForm form in widget.forms){
        allValid=allValid&&form.isValid();
        if(allValid){
          if(!dataList.contains(form.formUser))
          setState(() {
            dataList.add(form.formUser);
          });
        }
      }
      }
  }
  void onSave() {

    if (widget.forms.length > 0) {
      var allValid = true;
      for(UserForm form in widget.forms){
        allValid = allValid&&form.isValid();}
        if(allValid){
         // if(!dataList.contains(form.formUser)){
         //   setState(() {
         //     dataList.add(form.formUser);
         //   });
         // }
          print('all valid');
      }
     /* widget.forms.forEach((form) => allValid = allValid && form.isValid());
      if (allValid) {
        var data = widget.forms.map((it) => it.formUser).toList();
        for(User user in data){
          if(!dataList.contains(user))
            setState(() {
              dataList.add(user);
            });
        }

      */
       /* Navigator.push(
          context,
          MaterialPageRoute(
            fullscreenDialog: true,
            builder: (_) => Scaffold(
              appBar: AppBar(
                title: Text('List of Users'),
              ),
              body: ListView.builder(
                itemCount: widget.forms.length,
                itemBuilder: (_, i) => widget.forms[i]
                /*ListTile(
                  leading: CircleAvatar(
                    child: Text(dataList[i].name.substring(0, 1)),
                  ),
                  title: Text(dataList[i].name),
                  subtitle: Text(dataList[i].password),
                ),*/
              ),
            ),
          ),
        );
*/
    }
  }

}
