
import 'package:flutter/material.dart';
import 'package:listforms/user.dart';

typedef OnDelete ();
class UserForm extends StatefulWidget {
  UserForm({Key key,this.formUser,this.onRemove}):super(key: key);

  final User formUser;
  var state = _UserFormState();
  final OnDelete onRemove;

   @override
   _UserFormState createState() => this.state = new _UserFormState();
   bool isValid ()=> this.state.onSave();
}

class _UserFormState extends State<UserForm> {
  final form = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/f.png"),
                fit: BoxFit.cover
            )
        ),
        child: Card(
          child: Form(
              key: form,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  AppBar(
                    leading: Icon(Icons.group),
                    title: Text('User Data Form'),
                    centerTitle: true,
                    actions: <Widget>[
                      IconButton(icon: Icon(Icons.delete),
                        onPressed: widget.onRemove,
                      ),
                    ],
                  ),
                  Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextFormField(
                        //controller: _nameController,
                        autofocus: true,
                        initialValue: widget.formUser.name,
                        onChanged: (value){
                          setState(() {
                            widget.formUser.name = value;
                          });
                        },
                        onSaved: (val) => widget.formUser.name = val,
                        validator: (val) =>
                        val.length > 2 ? null : " Invalid name !!",
                        decoration: InputDecoration(
                            icon: Icon(Icons.account_box),
                            labelText: 'Name',
                            hintText: 'Enter name'
                        ),
                      )
                  ),
                  Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextFormField(
                        //controller: _passwordController,
                        initialValue: widget.formUser.password,
                        onChanged: (value){
                          setState(() {
                            widget.formUser.password = value;
                          });
                        },
                        onSaved: (val) => widget.formUser.password =val,
                        validator: (val) =>
                        val.length >= 6 ? null : "Invalid !! Password too short!!",
                        decoration: InputDecoration(
                            icon: Icon(Icons.lock),
                            labelText: 'Password',
                            hintText: 'Enter password'
                        ),
                      )
                  ),
                  SizedBox(height: 5.0,)
                ],
              )),
        ),
      ),
    );
  }
  bool onSave() {
    if(form.currentState!=null){
    var isValid = form.currentState.validate();
    if(isValid) {
      form.currentState.save();
    }
    return isValid;
  }return false;
}}
